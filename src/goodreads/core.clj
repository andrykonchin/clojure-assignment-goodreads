(ns goodreads.core
  (:gen-class)
  (:require [clojure.tools.cli :as cli]
            [manifold.deferred :as d]
            [clj-http.client :as http-client]
            [clj-xpath.core :as xpath]))

;; This implementation is pretty useless :(
;; (defn build-recommentations [_]
;;   (d/success-deferred
;;    [{:title "My Side of the Mountain (Mountain, #1)"
;;      :authors [{:name "Jean Craighead George"}]
;;      :link "https://www.goodreads.com/book/show/41667.My_Side_of_the_Mountain"}
;;     {:title "Incident at Hawk's Hill"
;;      :authors [{:name "Allan W. Eckert"}]
;;      :link "https://www.goodreads.com/book/show/438131.Incident_at_Hawk_s_Hill"}
;;     {:title "The Black Pearl"
;;      :authors [{:name "Scott O'Dell"}]
;;      :link "https://www.goodreads.com/book/show/124245.The_Black_Pearl"}]))

(defn build-recommentations [config]
  (defn load-read-books [token]
    (:body (http-client/get (str "https://www.goodreads.com/review/list?v=2&key=vu72u4bkAET2a2a0Jt8CoQ&id=" token "&shelf=read"))))
  (defn load-reading-books [token]
    (:body (http-client/get (str "https://www.goodreads.com/review/list?v=2&key=vu72u4bkAET2a2a0Jt8CoQ&id=" token "&shelf=currently-reading"))))
  (defn load-book [id]
    (:body (http-client/get (str "https://www.goodreads.com/book/show.xml?key=vu72u4bkAET2a2a0Jt8CoQ&id=" id))))
  (defn books-xml->ids [books-xml]
    (map #(:text %) (xpath/$x "/GoodreadsResponse/reviews/review/book/id" books-xml)))
  (defn book-xml->similar-books [book-xml]
    (defn node->book [node]
      (let [id (xpath/$x:text "./id" node)
            title (xpath/$x:text "./title" node)
            link (xpath/$x:text "./link" node)
            average_rating (xpath/$x:text "./average_rating" node)
            authors (map (fn [a] {:name (:text a)}) (xpath/$x "./authors/author/name" node))]
        {:id id :title title :link link :rating average_rating :authors authors}))
    (let [similar-books-nodes (xpath/$x "/GoodreadsResponse/book/similar_books/book" book-xml)]
      (map node->book similar-books-nodes)))
  (defn similar-books [ids]
    (set (apply concat (map book-xml->similar-books (map load-book ids)))))

  (let [user-token (:token config)
        read-books-xml (load-read-books user-token)
        reading-books-xml (load-reading-books user-token)
        read-book-ids (books-xml->ids read-books-xml)
        reading-book-ids (set (books-xml->ids reading-books-xml))
        read-book-ids-to-process (take 2 read-book-ids) ; <----------- here we cut the whole ids list for testing purpose
        similar-books-without-reading (remove #(contains? reading-book-ids (:id %))
                                              (similar-books read-book-ids-to-process))]
    (d/success-deferred
      (take 10 (reverse (sort-by #(:rating %) similar-books-without-reading))))))

(def cli-options [["-t"
                   "--timeout-ms"
                   "Wait before finished"
                   :default 5000
                   :parse-fn #(Integer/parseInt %)]
                  ["-n"
                   "--number-books"
                   "How many books do you want to recommend"
                   :default 10
                   :parse-fn #(Integer/parseInt %)]
                  ["-h" "--help"]])

(defn book->str [{:keys [title link authors]}]
  (format "\"%s\" by %s\nMore: %s"
          title
          (->> authors
               (map :name)
               (clojure.string/join ", "))
          link))

(defn -main [& args]
  (let [{:keys [options errors summary]} (cli/parse-opts args cli-options)]
    (cond
      (contains? options :help) (do (println summary) (System/exit 0))
      (some? errors) (do (println errors) (System/exit 1))
      (empty? args) (do (println "Please, specify user's token") (System/exit 1))
      :else (let [config {:token (first args)}
                  books (-> (build-recommentations config)
                            (d/timeout! (:timeout-ms options) ::timeout)
                            deref)]
              (cond
                (= ::timeout books) (println "Not enough time :(")
                (empty? books) (println "Nothing found, leave me alone :(")
                :else (doseq [[i book] (map-indexed vector books)]
                        (println (str "#" (inc i)))
                        (println (book->str book))
                        (println)))))))
